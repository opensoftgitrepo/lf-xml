# LF-XML

[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![Travis](https://img.shields.io/travis/alexjoverm/typescript-library-starter.svg)](https://travis-ci.org/alexjoverm/typescript-library-starter)
[![Coveralls](https://img.shields.io/coveralls/alexjoverm/typescript-library-starter.svg)](https://coveralls.io/github/alexjoverm/typescript-library-starter)

A LF plugin to transform both js value to xml and xml to js value.

### Instalation

```bash
npm install @lightweightform/xml
```

### Usage

```bash
import LfXml from '@lightweightform/xml';
const initialConfiguration: Configuration = {
  xmlVersion: '1.0',
  encoding: 'UTF-8',
  rootName: 'lf-xml',
  lfStorage: ${LF_STORAGE},
  rowSuffix: 'row',
  rowAttribute: 'index',
  formListAttributeSuffix: 'id',
  lfI18n: ${LF_I18N},
  mappings: { xmlName: {'/a/b': B}, jsName: {'/a/B': b} }
};
...
const converter = new LfXml(initialConfiguration);
...
const xml = converter.js2xml(jsValue);
const xmlString = converter.js2xmlString(jsValue);
const js = converter.xml2Js(xmlString);
```

### Methods

| Method       | Description                                                                              | Parameters  | Return Type         |
| ------------ | ---------------------------------------------------------------------------------------- | ----------- | ------------------- |
| js2Xml       | Converts a js object to an equivalent xml representation wrapped by a xml header element | js: any     | XMLElementOrXMLNode |
| js2XmlString | Converts a js object to an equivalent xml string                                         | js: any     | string              |
| xml2Js       | Converts a xml string to js object equivalent with lf schemaComplete representation      | xml: string | js: any             |

### Configuration

- **encoding**: Encoding of the xml object `(default="UTF-8")`
- **xmlVersion**: Version displayed in the xml object `(default="1.0")`
- **rootName**: Root element of the xml object `(default="lf-xml")`
- **headerAttributes**: Map with header attributes added into the root element of the xml object
- **rowSuffix**: Every list elements have a suffix name added into each table's row in the xml object. `(default="row")`
- **rowAttribute**: Every list elements have an additional attribute added into each table's row in the xml object. `(default="index")`
- **formListAttributeSuffix**: Every list elements of type "formList" have an additional attribute added to each formList entry in the xml object `(default="id")`
- **lfStorage**: Storage of the lf application
- **lfI18n**: Lf's i18n service of the lf application
- **mappings**: Map with element's path and name, needs two way mapping to rename element when converting js2xml and xml2js

## Contributing

Contributions are always welcome. Please read our
[contributing guide](./CONTRIBUTING.md) on how to contribute.

## License

LF-XML is released under the [Apache License 2.0](./LICENSE).
