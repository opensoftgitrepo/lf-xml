import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';
import typescript from 'rollup-plugin-typescript2';
import uglify from 'rollup-plugin-uglify';
import json from 'rollup-plugin-json';

const pkg = require('./package.json');
import camelCase from 'lodash.camelcase';

const libraryName = 'lf-xml';

const globals = {
  '@angular/common': 'ng.common',
  '@angular/core': 'ng.core',
  '@angular/forms': 'ng.forms',
  '@angular/router': 'ng.router',
  '@lightweightform/core': 'lightweightform.core',
  '@lightweightform/storage': 'lightweightform.storage',
  mobx: 'mobx',
  'mobx-utils': 'mobxUtils'
};
const plugins = [
  // Allow expectedJsComplete resolution
  json(),
  // Allow node_modules resolution, so you can use 'external' to control
  // which external modules to include in the bundle
  // https://github.com/rollup/rollup-plugin-node-resolve#usage
  resolve({ jsnext: true }),
  // Allow bundling cjs modules (unlike webpack, rollup doesn't understand cjs)
  commonjs(),
  // Compile TypeScript files
  typescript({
    clean: process.env.NODE_ENV === 'production',
    tsconfig: 'tsconfig.json'
  })
];

if (process.env.NODE_ENV === 'production') {
  plugins.push(uglify()); // Minify output
}
export default {
  input: `src/index.ts`,

  // Indicate here external modules you don't wanna include in your bundle (i.e.: 'lodash')
  external: Object.keys(globals),
  plugins,
  output: [
    { file: pkg.main, name: camelCase(libraryName), format: 'umd', sourcemap: true },
    { file: pkg.module, format: 'es', sourcemap: true }
  ],
  onwarn: (warning, warn) => {
    if (warning.source && warning.source.indexOf('@lightweightform') !== -1) {
      return;
    }
    if (
      warning.code === 'CIRCULAR_DEPENDENCY' &&
      warning.importer.indexOf('record.converter') !== -1
    ) {
      return;
    }
    warn(warning);
  },
  watch: {
    include: 'src/**'
  }
};
