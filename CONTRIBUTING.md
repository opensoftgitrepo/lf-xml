# Contributing

Contributions are always welcome. Feel free to create a new issue in
LF-XML's
[issue tracker](https://bitbucket.org/opensoftgitrepo/lf-xml/issues?status=new&status=open)
or to submit a pull request against the `master` branch.

## Developing

### Prerequisites:

- [Node.js and NPM](https://nodejs.org/)
- [Yarn (optional)](https://yarnpkg.com/)

### Setup

On the repository's root, run:

```bash
npm install
npm test
```

### NPM scripts

- `npm test`: Run test suite
- `npm start`: Run `npm run build` in watch mode
- `npm run test:watch`: Run test suite in [interactive watch mode](http://facebook.github.io/jest/docs/cli.html#watch)
- `npm run test:prod`: Run linting and generate coverage
- `npm run build`: Generate bundles and typings, create docs
- `npm run lint`: Lints code
