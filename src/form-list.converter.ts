import { Configuration } from './index';
import RecordConverter from './record.converter';
import { isComputedSchema, Storage } from '@lightweightform/storage';

export default class FormListConverter {
  static js2xml(
    configuration: Configuration,
    lfStorage: Storage,
    jsValue: any,
    xmlElement: any,
    name: string
  ) {
    jsValue.forEach((entry: any, index: number) => {
      if (configuration.formListSuffix !== null) {
        xmlElement.startElement(`${name}${configuration.formListSuffix}`);
      } else {
        xmlElement.startElement(`${name}`);
      }
      if (configuration.formListAttribute !== null) {
        let label: string | ((ctx: Storage) => string) = '';
        if (configuration.lfI18n !== null) {
          label = configuration.lfI18n.getFromPath(
            lfStorage.relativeStorage(index.toString()).currentPath,
            'label'
          );
          if (!label) {
            label = (index + 1).toString();
          }
        }
        const childStorage = lfStorage.relativeStorage(index.toString());
        if(childStorage.schema().xmlAttributeWriteMapper) {
          xmlElement.writeAttribute(
            configuration.formListAttribute,
            childStorage.schema().xmlAttributeWriteMapper(childStorage)
          );
        } else {
          xmlElement.writeAttribute(
            configuration.formListAttribute,
            typeof label === 'function' ? label(lfStorage.relativeStorage(index.toString())) : label
          );
        }
      }
      RecordConverter.js2xml(
        configuration,
        lfStorage.relativeStorage(index.toString()),
        entry,
        xmlElement
      );
      xmlElement.endElement();
    });
  }

  static xml2js(configuration: Configuration, lfStorage: Storage, xmlObject: any, path: string) {
    const schema = lfStorage.schema();
    if (isComputedSchema(schema)) {
      return;
    }
    lfStorage.push(path, {});
    const size = lfStorage.size(path);
    const childStorage = lfStorage.relativeStorage((size - 1).toString());
    RecordConverter.xml2js(configuration, childStorage, xmlObject, childStorage.currentPath);
    if (childStorage.schema().xmlAttributeReadMapper && xmlObject.attributes) {
      childStorage.schema().xmlAttributeReadMapper(childStorage, xmlObject.attributes.id);
    }
  }
}
