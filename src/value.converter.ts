import {
  isBooleanSchema,
  isComputedSchema,
  isDateSchema,
  isNullableSchema,
  isNumberSchema,
  Storage
} from '@lightweightform/storage';

export default class ValueConverter {
  static js2xml(name: string | null, jsValue: any, xmlElement: any) {
    xmlElement.writeElement(name, jsValue === null ? '' : jsValue.toString());
  }

  static xml2js(lfStorage: Storage, xmlObject: any, path: string) {
    const childSchema = lfStorage.schema(path);
    if (isComputedSchema(childSchema)) {
      return;
    }
    const nullValue = isNullableSchema(childSchema) ? null : '';
    const value = xmlObject.elements ? xmlObject.elements[0].text : nullValue;
    if (isNumberSchema(childSchema) && value !== null) {
      lfStorage.set(path, parseFloat(value));
    } else if (isBooleanSchema(childSchema) && value !== null) {
      lfStorage.set(path, value === 'true');
    } else if (isDateSchema(childSchema) && value !== null) {
      lfStorage.set(path, new Date(value));
    } else {
      lfStorage.set(path, value);
    }
  }
}
