import ValueConverter from './value.converter';
import ListConverter from './list.converter';
import { Configuration } from './index';
import FormListConverter from './form-list.converter';
import {
  appendToPath,
  isComputedSchema,
  isNullableSchema,
  Storage
} from '@lightweightform/storage';

export default class RecordConverter {
  static js2xml(
    configuration: Configuration,
    lfStorage: Storage,
    jsValue: any,
    xmlElement: any,
    name?: string
  ) {
    if (jsValue === null) {
      return;
    }
    if (name) {
      let xmlName: string = RecordConverter.mappingName(
        configuration.mappings ? configuration.mappings.xmlName : undefined,
        lfStorage.currentPath,
        name
      );
      xmlElement.startElement(xmlName);
    }
    Object.keys(jsValue).forEach((childPath: string) => {
      const fieldSchema = lfStorage.schema(childPath);
      let xmlName: string = RecordConverter.mappingName(
        configuration.mappings ? configuration.mappings.xmlName : undefined,
        lfStorage.relativeStorage(childPath).currentPath,
        childPath
      );

      if(fieldSchema.xmlWriter){
        fieldSchema.xmlWriter(lfStorage.relativeStorage(xmlName),xmlElement);
      }else {
        switch (fieldSchema.type) {
          case 'record':
            RecordConverter.js2xml(
              configuration,
              lfStorage.relativeStorage(childPath),
              jsValue[childPath],
              xmlElement,
              xmlName
            );
            break;
          case 'tuple':
          case 'table':
          case 'list':
            if (fieldSchema.isFormList) {
              FormListConverter.js2xml(
                configuration,
                lfStorage.relativeStorage(childPath),
                jsValue[childPath],
                xmlElement,
                xmlName
              );
            } else {
              ListConverter.js2xml(
                configuration,
                lfStorage.relativeStorage(childPath),
                jsValue[childPath],
                xmlElement,
                xmlName
              );
            }
            break;
          default:
            ValueConverter.js2xml(xmlName, jsValue[childPath], xmlElement);
            break;
        }
      }

    });
    if (name) {
      xmlElement.endElement();
    }
  }

  static xml2js(configuration: Configuration, lfStorage: Storage, xmlObject: any, path: string) {
    if (isComputedSchema(lfStorage.schema(path))) {
      return;
    }
    const xmlElements = xmlObject.elements || [];
    const formListsMap: Record<string, number> = {};
    xmlElements.forEach(element => {
      let jsName: string = RecordConverter.mappingName(
        configuration.mappings ? configuration.mappings.jsName : undefined,
        appendToPath(path, element.name),
        element.name
      );
      const childPath = appendToPath(path, jsName);
      // map used to register the form list actualization progress status
      const fieldSchema = lfStorage.schema(childPath);

      if(fieldSchema.xmlParser) {
        fieldSchema.xmlParser(lfStorage.relativeStorage(childPath), element);
      }else {
        switch (fieldSchema.type) {
          case 'record':
            if (isNullableSchema(fieldSchema)) {
              lfStorage.initialize(childPath);
            }
            RecordConverter.xml2js(
              configuration,
              lfStorage.relativeStorage(childPath),
              element,
              childPath
            );
            break;
          case 'tuple':
          case 'table':
          case 'list':
            if (fieldSchema.isFormList) {
              if (!formListsMap[childPath] && !isComputedSchema(lfStorage.schema(childPath))) {
                formListsMap[childPath] = 0;
                lfStorage.set(childPath, []);
              }
              FormListConverter.xml2js(
                configuration,
                lfStorage.relativeStorage(childPath),
                element,
                childPath
              );
              formListsMap[childPath]++;
            } else {
              ListConverter.xml2js(
                configuration,
                lfStorage.relativeStorage(childPath),
                element,
                childPath
              );
            }
            break;
          default:
            ValueConverter.xml2js(lfStorage.relativeStorage(childPath), element, childPath);
            break;
        }
      }
    });
  }

  static mappingName(mappings: Record<string, string> | undefined, path: string, name: string) {
    let mappedName: string | null = null;
    if (mappings) {
      mappedName = mappings[path];

      if(mappedName == null) {
        let generic: string | null = null;
          Object.keys(mappings).forEach(function (genericPath) {
            let regex: RegExpExecArray | null = new RegExp(genericPath)!.exec(path);
            // Comparing this two attributes allows testing if regex is an exact match
            // and it is only considered the first exact match
            if(regex && regex[0] === regex.input) {
              generic = genericPath;
              return;
            }
        });

        if (generic != null) {
          mappedName = mappings[generic]
        }
      }
    }
    if (!mappedName) {
      mappedName = name;
    }
    return mappedName;
  }
}
