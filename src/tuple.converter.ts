import ValueConverter from './value.converter';
import { Storage } from '@lightweightform/storage';

export default class TupleConverter {
  static js2xml(jsValue: any, xmlElement: any) {
    xmlElement.text(jsValue);
  }

  static xml2js(lfStorage: Storage, xmlObject: any, path: string) {
    ValueConverter.xml2js(lfStorage, xmlObject, path);
  }
}
