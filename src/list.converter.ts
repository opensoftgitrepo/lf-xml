import { Configuration } from './index';
import RecordConverter from './record.converter';
import CheckboxGroupConverter from './checkbox-group.converter';
import TupleConverter from './tuple.converter';
import {getRowId, isComputedSchema, Storage} from '@lightweightform/storage';

export default class ListConverter {
  static js2xml(
    configuration: Configuration,
    lfStorage: Storage,
    jsValue: any,
    xmlElement: any,
    name: string
  ) {
    xmlElement.startElement(name);
    if (jsValue !== null) {
      jsValue.forEach((entry: any, index: number) => {
        if (configuration.rowSuffix !== null) {
          xmlElement.startElement(`${name}${configuration.rowSuffix}`);
        } else {
          xmlElement.startElement(`${name}`);
        }
        if (configuration.rowAttribute !== null) {
          xmlElement.writeAttribute(configuration.rowAttribute, index + 1);
        }
        if (lfStorage.schema().type === 'tuple') {
          TupleConverter.js2xml(entry, xmlElement);
        } else {
          if (lfStorage.schema().rowsSchema || lfStorage.schema().elementsSchema.type === 'record') {
            RecordConverter.js2xml(
              configuration,
              lfStorage.relativeStorage(index.toString()),
              entry,
              xmlElement
            );
          }else{
            CheckboxGroupConverter.js2xml(entry, xmlElement);
          }
        }
        xmlElement.endElement();
      });
    } else {
      xmlElement.text('');
    }
    xmlElement.endElement();
  }

  static xml2js(configuration: Configuration, lfStorage: Storage, xmlObject: any, path: string) {
    const schema = lfStorage.schema();
    if (isComputedSchema(schema)) {
      return;
    }
    if (xmlObject.elements) {
      lfStorage.set(path, []);
      xmlObject.elements.forEach((row: any, index: number) => {
        let childStorage = lfStorage.relativeStorage(index.toString());
        if (schema.type === 'tuple') {
          TupleConverter.xml2js(lfStorage, row, childStorage.currentPath);
        } else {
          if (schema.rowsSchema) {
            lfStorage.push(undefined, undefined);
            const table = lfStorage.get(lfStorage.relativeStorage().currentPath);
            childStorage = lfStorage.relativeStorage(getRowId(table[table.length - 1]).toString());
            RecordConverter.xml2js(configuration, childStorage, row, childStorage.currentPath);
          }
          else if(schema.elementsSchema.type === 'record') {
            lfStorage.push(undefined, undefined);
            RecordConverter.xml2js(configuration, childStorage, row, childStorage.currentPath);
          } else {
            lfStorage.push();
            CheckboxGroupConverter.xml2js(childStorage, row, childStorage.currentPath);
          }
        }
      });
    } else {
      lfStorage.reset(path);
    }
  }
}
