import { xml2js } from 'xml-js';
import RecordConverter from './record.converter';
import { Storage } from '@lightweightform/storage';
import { LfI18n } from '@lightweightform/core';

const XMLWriter = require('xml-writer');

const INVALID_STORAGE_MESSAGE =
  '[LF-XML] You must pass lf-storage to xml configuration before call js<->xml parsers';
/**
 * Configutation of the plugin
 *  All the information necessary to convert the object to xml is passed to that object
 *  in the initialization of the service
 */
export interface Configuration {
  encoding?: string;
  xmlVersion?: string;
  rootName: string;
  headerAttributes?: Record<string, string>;
  rowSuffix?: string | null;
  rowAttribute?: string | null;
  formListSuffix?: string | null;
  formListAttribute?: string | null;
  lfStorage: Storage | null;
  lfI18n: LfI18n | null;
  mappings?: Record<string, Record<string, string>>;
}

export default class LfXml {
  private readonly config: Configuration = {
    // defaults
    xmlVersion: '1.0',
    encoding: 'UTF-8',
    rootName: 'lf-xml',
    lfStorage: null,
    rowSuffix: '-index',
    rowAttribute: 'row',
    formListSuffix: null,
    formListAttribute: 'id',
    lfI18n: null,
    mappings: {
      xmlName: {},
      jsName: {}
    }
  };

  private _xmlWriter = new XMLWriter(' ');

  constructor(config: Configuration) {
    Object.assign(this.config, config);
  }

  /**
   * Converts a js object to an equivalent xml representation wrapped by a xml
   * header element
   *
   * This method is util if you want to add some transformations over the xml created
   *
   * @param value
   */
  public js2xml(value: any): Document {
    this._xmlWriter = new XMLWriter(' ');
    this.createXmlHeader();
    if (!this.config.lfStorage) {
      throw new Error(INVALID_STORAGE_MESSAGE);
    }
    RecordConverter.js2xml(this.config, this.config.lfStorage, value, this._xmlWriter);
    return this._xmlWriter;
  }

  /**
   * Converts a js object to an equivalent xml string
   * @param value
   */
  public js2xmlString(value: any): string {
    return LfXml.printXml(this.js2xml(value));
  }

  /**
   * Converts a xml string to js object equivalent with lf schemaComplete representation
   * @param value
   */
  public xml2js(value: string) {
    if (!this.config.lfStorage) {
      throw new Error(INVALID_STORAGE_MESSAGE);
    }
    const xmlObject: any = (xml2js(
      value.substr(
        value.indexOf(`<${this.config.rootName}`),
        value.indexOf(`</${this.config.rootName}>`)
      )
    ) as any).elements[0]; //gets element inside rootElement
    this.config.lfStorage.reset();
    RecordConverter.xml2js(this.config, this.config.lfStorage, xmlObject, '/');
  }

  /**
   * Create the XML Header based on the configuration passed in the service instantiation
   *
   * The xml header follows the following format:
   * <xml version="${config.xmlVersion}" encoding="${config.encodign}">
   *     <${config.rootName} ...${this.config.headerAttributes}></${config.rootName}>
   */
  private createXmlHeader() {
    this._xmlWriter.startDocument(this.config.xmlVersion, this.config.encoding);
    this._xmlWriter.startElement(this.config.rootName);

    if (this.config.headerAttributes) {
      const headers: Record<string, string> = this.config.headerAttributes;
      Object.keys(headers).forEach(key => {
        this._xmlWriter.writeAttribute(key, headers[key]);
      });
    }
  }

  /**
   * Print and prettify the xml element.
   *
   * @param xml
   */
  static printXml(xml: any): string {
    return xml.toString();
  }
}
