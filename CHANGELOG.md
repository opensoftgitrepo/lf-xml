# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="4.0.0-beta.16"></a>
# 4.0.0-beta.16 (2019-05-15)


### Bug Fixes

* fix build:production ([75800fa](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/75800fa))
* fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
* fix lfXml initialization error ([c4260ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c4260ec))
* Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
* mappings allow regex ([890a3dd](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/890a3dd))
* minor fixes ([abe971d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/abe971d))
* Prevent formList id to be the label of form ([3b291d5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/3b291d5))
* regex of mapping must be the exact match ([b34e3de](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b34e3de))
* remove hiphen from form list and list suffixes ([187c97f](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/187c97f))
* remove xml duplication after successive executions ([819a101](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/819a101)), closes [#6](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/6)
* remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)
* support to tableSchema ([6a5b8bf](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/6a5b8bf))
* tests from previous changes ([290c46d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/290c46d))
* translation names uses a map declared in initial configuration ([674220e](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/674220e))
* using separation groups in mappings ([487c12e](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/487c12e))


### Features

* add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
* add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)
* support xml attribute parsing ([2ac8207](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/2ac8207)), closes [#7](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/7)
* supports name elements translation using i18n service ([5c3c142](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/5c3c142))



<a name="4.0.0-beta.15"></a>
# 4.0.0-beta.15 (2019-05-14)


### Bug Fixes

* fix build:production ([75800fa](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/75800fa))
* fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
* fix lfXml initialization error ([c4260ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c4260ec))
* Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
* mappings allow regex ([890a3dd](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/890a3dd))
* minor fixes ([abe971d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/abe971d))
* Prevent formList id to be the label of form ([3b291d5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/3b291d5))
* regex of mapping must be the exact match ([b34e3de](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b34e3de))
* remove hiphen from form list and list suffixes ([187c97f](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/187c97f))
* remove xml duplication after successive executions ([819a101](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/819a101)), closes [#6](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/6)
* remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)
* support to tableSchema ([6a5b8bf](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/6a5b8bf))
* tests from previous changes ([290c46d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/290c46d))
* translation names uses a map declared in initial configuration ([674220e](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/674220e))
* using separation groups in mappings ([487c12e](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/487c12e))


### Features

* add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
* add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)
* support xml attribute parsing ([2ac8207](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/2ac8207)), closes [#7](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/7)
* supports name elements translation using i18n service ([5c3c142](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/5c3c142))



<a name="4.0.0-beta.14"></a>
# 4.0.0-beta.14 (2019-04-24)


### Bug Fixes

* fix build:production ([75800fa](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/75800fa))
* fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
* fix lfXml initialization error ([c4260ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c4260ec))
* Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
* mappings allow regex ([890a3dd](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/890a3dd))
* minor fixes ([abe971d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/abe971d))
* Prevent formList id to be the label of form ([3b291d5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/3b291d5))
* regex of mapping must be the exact match ([b34e3de](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b34e3de))
* remove hiphen from form list and list suffixes ([187c97f](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/187c97f))
* remove xml duplication after successive executions ([819a101](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/819a101)), closes [#6](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/6)
* remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)
* tests from previous changes ([290c46d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/290c46d))
* translation names uses a map declared in initial configuration ([674220e](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/674220e))
* using separation groups in mappings ([487c12e](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/487c12e))


### Features

* add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
* add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)
* support xml attribute parsing ([2ac8207](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/2ac8207)), closes [#7](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/7)
* supports name elements translation using i18n service ([5c3c142](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/5c3c142))



<a name="4.0.0-beta.13"></a>
# 4.0.0-beta.13 (2019-04-16)


### Bug Fixes

* fix build:production ([75800fa](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/75800fa))
* fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
* fix lfXml initialization error ([c4260ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c4260ec))
* Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
* mappings allow regex ([890a3dd](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/890a3dd))
* minor fixes ([abe971d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/abe971d))
* Prevent formList id to be the label of form ([3b291d5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/3b291d5))
* remove hiphen from form list and list suffixes ([187c97f](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/187c97f))
* remove xml duplication after successive executions ([819a101](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/819a101)), closes [#6](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/6)
* remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)
* tests from previous changes ([290c46d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/290c46d))
* translation names uses a map declared in initial configuration ([674220e](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/674220e))
* using separation groups in mappings ([487c12e](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/487c12e))


### Features

* add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
* add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)
* support xml attribute parsing ([2ac8207](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/2ac8207)), closes [#7](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/7)
* supports name elements translation using i18n service ([5c3c142](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/5c3c142))



<a name="4.0.0-beta.12"></a>
# 4.0.0-beta.12 (2019-03-26)


### Bug Fixes

* fix build:production ([75800fa](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/75800fa))
* fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
* fix lfXml initialization error ([c4260ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c4260ec))
* Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
* minor fixes ([abe971d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/abe971d))
* Prevent formList id to be the label of form ([3b291d5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/3b291d5))
* remove hiphen from form list and list suffixes ([187c97f](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/187c97f))
* remove xml duplication after successive executions ([819a101](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/819a101)), closes [#6](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/6)
* remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)
* tests from previous changes ([290c46d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/290c46d))
* translation names uses a map declared in initial configuration ([674220e](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/674220e))
* using separation groups in mappings ([487c12e](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/487c12e))


### Features

* add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
* add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)
* support xml attribute parsing ([2ac8207](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/2ac8207)), closes [#7](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/7)
* supports name elements translation using i18n service ([5c3c142](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/5c3c142))



<a name="4.0.0-beta.11"></a>

# 4.0.0-beta.11 (2019-03-26)

### Bug Fixes

- fix build:production ([75800fa](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/75800fa))
- fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
- fix lfXml initialization error ([c4260ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c4260ec))
- Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
- minor fixes ([abe971d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/abe971d))
- Prevent formList id to be the label of form ([3b291d5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/3b291d5))
- remove hiphen from form list and list suffixes ([187c97f](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/187c97f))
- remove xml duplication after successive executions ([819a101](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/819a101)), closes [#6](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/6)
- remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)
- tests from previous changes ([290c46d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/290c46d))
- translation names uses a map declared in initial configuration ([674220e](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/674220e))

### Features

- add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)
- support xml attribute parsing ([2ac8207](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/2ac8207)), closes [#7](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/7)
- supports name elements translation using i18n service ([5c3c142](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/5c3c142))

<a name="4.0.0-beta.10"></a>

# 4.0.0-beta.10 (2019-03-21)

### Bug Fixes

- fix build:production ([75800fa](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/75800fa))
- fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
- fix lfXml initialization error ([c4260ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c4260ec))
- Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
- minor fixes ([abe971d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/abe971d))
- Prevent formList id to be the label of form ([3b291d5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/3b291d5))
- remove hiphen from form list and list suffixes ([187c97f](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/187c97f))
- remove xml duplication after successive executions ([819a101](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/819a101)), closes [#6](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/6)
- remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)
- tests from previous changes ([290c46d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/290c46d))

### Features

- add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)
- support xml attribute parsing ([2ac8207](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/2ac8207)), closes [#7](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/7)
- supports name elements translation using i18n service ([5c3c142](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/5c3c142))

<a name="4.0.0-beta.9"></a>

# 4.0.0-beta.9 (2019-02-11)

### Bug Fixes

- fix build:production ([75800fa](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/75800fa))
- fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
- fix lfXml initialization error ([c4260ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c4260ec))
- Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
- minor fixes ([abe971d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/abe971d))
- Prevent formList id to be the label of form ([3b291d5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/3b291d5))
- remove hiphen from form list and list suffixes ([187c97f](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/187c97f))
- remove xml duplication after successive executions ([819a101](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/819a101)), closes [#6](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/6)
- remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)
- tests from previous changes ([290c46d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/290c46d))

### Features

- add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)
- support xml attribute parsing ([2ac8207](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/2ac8207)), closes [#7](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/7)

<a name="4.0.0-beta.8"></a>

# 4.0.0-beta.8 (2019-02-08)

### Bug Fixes

- fix build:production ([75800fa](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/75800fa))
- fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
- Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
- minor fixes ([abe971d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/abe971d))
- Prevent formList id to be the label of form ([3b291d5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/3b291d5))
- remove hiphen from form list and list suffixes ([187c97f](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/187c97f))
- remove xml duplication after successive executions ([819a101](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/819a101)), closes [#6](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/6)
- remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)
- tests from previous changes ([290c46d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/290c46d))

### Features

- add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)
- support xml attribute parsing ([2ac8207](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/2ac8207)), closes [#7](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/7)

<a name="4.0.0-beta.7"></a>

# 4.0.0-beta.7 (2019-02-08)

### Bug Fixes

- fix build:production ([75800fa](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/75800fa))
- fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
- Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
- Prevent formList id to be the label of form ([3b291d5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/3b291d5))
- remove hiphen from form list and list suffixes ([187c97f](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/187c97f))
- remove xml duplication after successive executions ([819a101](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/819a101)), closes [#6](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/6)
- remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)
- tests from previous changes ([290c46d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/290c46d))

### Features

- add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)
- support xml attribute parsing ([2ac8207](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/2ac8207)), closes [#7](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/7)

<a name="4.0.0-beta.6"></a>

# 4.0.0-beta.6 (2019-02-07)

### Bug Fixes

- fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
- Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
- Prevent formList id to be the label of form ([3b291d5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/3b291d5))
- remove hiphen from form list and list suffixes ([187c97f](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/187c97f))
- remove xml duplication after successive executions ([819a101](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/819a101)), closes [#6](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/6)
- remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)
- tests from previous changes ([290c46d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/290c46d))

### Features

- add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)
- support xml attribute parsing ([2ac8207](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/2ac8207)), closes [#7](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/7)

<a name="4.0.0-beta.5"></a>

# 4.0.0-beta.5 (2019-01-07)

### Bug Fixes

- fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
- Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
- Prevent formList id to be the label of form ([3b291d5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/3b291d5))
- remove hiphen from form list and list suffixes ([187c97f](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/187c97f))
- remove xml duplication after successive executions ([819a101](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/819a101)), closes [#6](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/6)
- remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)
- tests from previous changes ([290c46d](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/290c46d))

### Features

- add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)

<a name="4.0.0-beta.4"></a>

# 4.0.0-beta.4 (2018-11-27)

### Bug Fixes

- fix form list converter ([a213a1b](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/a213a1b)), closes [#5](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/5)
- Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
- remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)

### Features

- add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)

<a name="4.0.0-beta.3"></a>

# 4.0.0-beta.3 (2018-11-19)

### Bug Fixes

- fix on form-list converter. Add more test scenarios. ([5126499](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/5126499))
- Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
- remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)

### Features

- add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)

<a name="4.0.0-beta.2"></a>

# 4.0.0-beta.2 (2018-11-19)

### Bug Fixes

- fix on form-list converter. Add more test scenarios. ([5126499](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/5126499))
- Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
- remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)

### Features

- add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)

<a name="4.0.0-beta.1"></a>

# 4.0.0-beta.1 (2018-11-19)

### Bug Fixes

- fix on form-list converter. Add more test scenarios. ([5126499](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/5126499))
- Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
- remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)

### Features

- add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)

<a name="4.0.0-beta.0"></a>

# 4.0.0-beta.0 (2018-11-13)

### Bug Fixes

- Improve conversion of boolean values, formlist and null value of empty forms ([c962238](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/c962238)), closes [#2](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/2)
- remove xmlbuilder dependency. ([990d015](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/990d015)), closes [#3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/3)

### Features

- add converter to tuples ([b9a0bf3](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/b9a0bf3)), closes [#4](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/4)
- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)

<a name="4.0.0-alpha.3"></a>

# 4.0.0-alpha.3 (2018-10-16)

### Features

- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)

<a name="4.0.0-alpha.2"></a>

# 4.0.0-alpha.2 (2018-10-16)

### Features

- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/src/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/src/issue/1)

<a name="4.0.0-alpha.1"></a>

# 4.0.0-alpha.1 (2018-09-14)

### Features

- add lf-xml converters ([7d454ec](https://bitbucket.org/opensoftgitrepo/lf-xml/commits/7d454ec)), closes [#1](https://bitbucket.org/opensoftgitrepo/lf-xml/issue/1)
