import {
  booleanSchema,
  listSchema,
  numberSchema,
  recordSchema,
  Storage,
  stringSchema
} from '@lightweightform/storage';

export default {
  schema: recordSchema(
    {
      fields: recordSchema({
        checkbox: recordSchema({
          checkbox: booleanSchema()
        }),
        table: recordSchema({
          table: listSchema(
            recordSchema({
              firstName: stringSchema(),
              lastName: stringSchema(),
              age: numberSchema({ isInteger: true, isNullable: true, min: 18, max: 99 }),
              married: booleanSchema(),
              profession: stringSchema()
            })
          )
        })
      }),
      formList: listSchema(
        recordSchema({
          text: stringSchema()
        }),
        { isFormList: true, maxSize: 3 }
      )
    },
    {
      initialValue: {
        fields: {
          checkbox: {
            checkbox: true
          },
          table: {
            table: [
              {
                firstName: 'John',
                lastName: 'Smith',
                age: null,
                married: true,
                profession: 'singer'
              },
              {
                firstName: 'Marie',
                lastName: 'Currie',
                age: 50,
                married: false,
                profession: 'dancer'
              }
            ]
          }
        },
        formList: [
          {
            text: 'ABC'
          },
          {
            text: 'Foo'
          },
          {
            text: 'bar'
          }
        ]
      }
    }
  ),
  lfI18n: {
    'pt-PT': {
      '/formList/?': {
        label: (ctx: Storage) => ctx.get('text')
      }
    }
  }
};
