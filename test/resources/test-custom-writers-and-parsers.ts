import {
  numberSchema,
  recordSchema,
  stringSchema,
  Storage,
  listSchema
} from '@lightweightform/storage';
import { dateRangeSchema } from './components-catalog/utils/date-range-schema';
import { format, parseISO } from 'date-fns';

export default {
  schema: recordSchema(
    {
      record: recordSchema({
        value: numberSchema({
          isNullable: true,
          xmlWriter: (ctx: Storage, xmlElement: any) => {
            xmlElement.writeElement('value', ctx.get() / 100);
          },
          xmlParser: (ctx: Storage, xmlElement: any) => {
            ctx.set(undefined, Math.floor(Number.parseFloat(xmlElement.elements[0].text) * 100));
          }
        }),
        record: recordSchema(
          {
            subRecord: stringSchema({})
          },
          {
            isNullable: true,
            xmlWriter: (ctx: Storage, xmlElement: any) => {
              const value = ctx.get() === null || ctx.get('subRecord') === null ? 'bla' : ctx.get();
              xmlElement.writeElement('record', value);
            },
            xmlParser: (ctx: Storage, xmlElement: any) => {
              const xmlValue = xmlElement.elements[0].text;
              if (xmlValue === 'bla') {
                ctx.initialize();
              } else {
                ctx.set(undefined, { subRecord: xmlValue });
              }
            }
          }
        ),
        list: listSchema(
          recordSchema({
            text: stringSchema()
          }),
          {
            isFormList: true,
            maxSize: 3,
            xmlWriter: (ctx: Storage, xmlElement: any) => {
              xmlElement.startElement('list');
              ctx.get().forEach((entry: any, index: number) => {
                xmlElement.startElement('text');
                xmlElement.writeAttribute(
                  'row',
                  (index +1)
                );
                xmlElement.text(entry.text);
                xmlElement.endElement();
              });
              xmlElement.endElement();
            },
            xmlParser: (ctx: Storage, xmlElement: any) => {
              let jsValue: any[] = [];
              xmlElement.elements.forEach(element => {
                jsValue.push({text: element.elements[0].text});
              });
              ctx.set(undefined, jsValue);
            },
          }
        ),
        checkboxGroup: listSchema(stringSchema({ allowedValues: ['1', '2', '3'] }), {
          initialValue: ['1', '2', '3'],
          xmlWriter: (ctx: Storage, xmlElement: any) => {
            let xmlString = '';
            const jsValue = ctx.get();
            jsValue.forEach((entry, index) => {
              xmlString += entry;
              if(index < jsValue.length - 1){
                xmlString += ',';
              }
            });
            xmlElement.writeElement('checkboxGroup', xmlString);
          },
          xmlParser: (ctx: Storage, xmlElement: any) => {
            const jsValue: any[] = [];
            const xmlElements = xmlElement.elements[0].text.split(',');
            xmlElements.forEach((element) => {
              jsValue.push(element);
            });
            ctx.set(undefined, jsValue);
          }
        }),
        dateRange: dateRangeSchema({
          isNullable: true,
          xmlWriter: (ctx: Storage, xmlElement: any) => {
            const [date1, date2] = ctx.get();
            xmlElement.writeElement(
              'dateRange',
              `${format(date2, 'yyyy-MM-dd')} - ${format(date1, 'yyyy-MM-dd')}`
            );
          },
          xmlParser: (ctx: Storage, xmlElement: any) => {
            const dates = xmlElement.elements[0].text.split(' - ');
            if (dates.length === 2) {
              ctx.set(undefined, [parseISO(dates[1]), parseISO(dates[0])]);
            }
          }
        })
      })
    },
    {
      initialValue: {
        record: {
          value: 12345,
          record: null as any,
          list: [{text: 'foo'}, {text: 'bar'}],
          checkboxGroup: ['a','b','c'],
          dateRange: [parseISO('2019-01-01'), parseISO('2019-02-02')]
        }
      }
    }
  )
};
