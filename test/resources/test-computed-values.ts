import { listSchema, numberSchema, recordSchema, stringSchema } from '@lightweightform/storage';

export default {
  schema: recordSchema({
    record: recordSchema(
      {
        text: stringSchema()
      },
      {
        computedValue: () => {
          return { text: 'original' };
        },
        isClientOnly: false
      }
    ),
    record2: recordSchema({
      number: numberSchema({ computedValue: () => 10 })
    }),
    formList: listSchema(
      recordSchema({
        text: stringSchema()
      }),
      {
        isFormList: true,
        maxSize: 3,
        computedValue: () => [{ text: 'A' }, { text: 'B' }],
        isClientOnly: false
      }
    )
  })
};
