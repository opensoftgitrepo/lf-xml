import { booleanSchema, numberSchema, recordSchema, stringSchema } from '@lightweightform/storage';
import { dateRangeSchema } from './components-catalog/utils/date-range-schema';

export default {
  schema: recordSchema(
    {
      record: recordSchema({
        a: numberSchema({ isNullable: true }),
        b: stringSchema(),
        c: booleanSchema(),
        d: numberSchema({ isNullable: true }),
        e: dateRangeSchema({ isNullable: true })
      })
    },
    {
      initialValue: {
        record: {
          a: 5,
          b: 'bla',
          c: true,
          d: null as any,
          e: null as any
        }
      }
    }
  )
};
