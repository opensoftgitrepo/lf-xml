import {recordSchema, stringSchema, listSchema} from '@lightweightform/storage';

export default {
  schema: recordSchema(
    {
      record: recordSchema({
        table: listSchema(
          recordSchema({
            name: stringSchema()
          }),
          { minSize: 2, maxSize: 5 }
        )
      })
    },
    {
      initialValue: {
        record: {
          table: [{ name: 'bla' }, { name: 'foo' }, { name: 'john' }, { name: 'maria' }]
        }
      }
    }
  )
};
