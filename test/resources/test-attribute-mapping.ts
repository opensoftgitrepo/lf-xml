import {
  listSchema,
  numberSchema,
  recordSchema,
  stringSchema,
  Storage
} from '@lightweightform/storage';

export default {
  schema: recordSchema(
    {
      record: recordSchema({
        a: numberSchema()
      }),
      formList: listSchema(
        recordSchema(
          {
            formInitializer: recordSchema(
              {
                formId: stringSchema()
              },
              { isClientOnly: true }
            )
          },
          {
            xmlAttributeWriteMapper: (ctx: Storage) =>
              ctx.get('formInitializer/formId')+'1',
            xmlAttributeReadMapper: (ctx: Storage, attributeId: string) =>
              ctx.set('formInitializer/formId', attributeId)
          }
        ),
        {
          isFormList: true,
          initialValue: [{ formInitializer: { formId: 'A' } }, { formInitializer: { formId: 'B' } }]
        }
      )
    },
    {
      initialValue: {
        record: {
          a: 5
        }
      }
    }
  ),
  lfI18n: {
    'pt-PT': {
      '/formList/?': {
        label: (ctx: Storage) => ctx.get('formInitializer/formId')
      }
    }
  }
};
