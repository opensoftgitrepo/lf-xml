import { numberSchema, recordSchema, stringSchema } from '@lightweightform/storage';

export const formInitializerForm = recordSchema(
  {
    select: stringSchema({
      isNullable: true,
      isRequired: true,
      allowedValues: ['A', 'B', 'C']
    }),
    selectNumber: numberSchema({
      isNullable: true,
      isRequired: true,
      allowedValues: [1, 2, 3]
    })
  },
  {
    isClientOnly: true,
    isForm: true,
    isFormInitializer: true,
    hideOnNav: true
  }
);
