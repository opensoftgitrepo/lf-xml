import { recordSchema, stringSchema } from '@lightweightform/storage';

export const subForm = recordSchema(
  {
    text: stringSchema({ minLength: 10 })
  },
  { isForm: true }
);
