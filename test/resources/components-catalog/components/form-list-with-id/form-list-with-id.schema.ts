import { listSchema, recordSchema } from '@lightweightform/storage';
import { formInitializerForm } from './form-initializer/form-initializer.schema';
import { subForm } from './sub-form/sub-form.schema';

export const formListWithIdForm = listSchema(
  recordSchema({
    subForm,
    formInitializerForm
  }),
  { minSize: 1, isFormList: true }
);
