import { recordSchema, stringSchema } from '@lightweightform/storage';

export const formInitializerForm = recordSchema(
  {
    formId: stringSchema({})
  },
  {
    isClientOnly: true,
    isForm: true,
    isFormInitializer: true,
    hideOnNav: true
  }
);
