import { recordSchema } from '@lightweightform/storage';
import { subForm } from '../common/sub-form/sub-form.schema';
import { formInitializerForm } from './form-initializer/form-initializer.schema';

export const optionalWithIdForm = recordSchema(
  {
    formInitializerForm,
    subForm
  },
  { isForm: true, isNullable: true }
);
