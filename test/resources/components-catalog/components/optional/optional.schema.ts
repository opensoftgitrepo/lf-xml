import { recordSchema } from '@lightweightform/storage';
import { subForm } from '../common/sub-form/sub-form.schema';

export const optionalForm = recordSchema(
  {
    subForm
  },
  { isForm: true, isNullable: true }
);
