import { recordSchema, stringSchema } from '@lightweightform/storage';

export const subForm = recordSchema(
  {
    text: stringSchema({})
  },
  {
    isForm: true
  }
);
