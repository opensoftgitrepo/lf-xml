import { listSchema, recordSchema, stringSchema } from '@lightweightform/storage';

export const formListForm = listSchema(
  recordSchema({
    subForm: recordSchema(
      {
        text: stringSchema({})
      },
      { isForm: true }
    )
  }),
  { minSize: 1, isFormList: true }
);
