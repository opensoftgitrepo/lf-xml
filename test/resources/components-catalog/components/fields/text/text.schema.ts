import { booleanSchema, recordSchema, stringSchema } from '@lightweightform/storage';
import { fieldValidator } from '../../../schema.utils';

export const textForm = recordSchema(
  {
    text: stringSchema({
      validator: fieldValidator
    }),
    textComputed: stringSchema({
      computedValue: ctx =>
        ctx
          .get('text')
          .split('')
          .reverse()
          .join('')
    }),
    isValid: booleanSchema({ initialValue: true }),
    hasWarning: booleanSchema({ initialValue: false }),
    isReadOnly: booleanSchema({ initialValue: false })
  },
  { isForm: true }
);
