import { recordSchema } from '@lightweightform/storage';
import { checkboxgroupForm } from './checkbox-group/checkbox-group.schema';
import { checkboxForm } from './checkbox/checkbox.schema';
import { daterangeForm } from './date-range/date-range.schema';
import { dateForm } from './date/date.schema';
import { ibanForm } from './iban/iban.schema';
import { nifForm } from './nif/nif.schema';
import { numberForm } from './number/number.schema';
import { radioForm } from './radio/radio.schema';
import { selectmultipleForm } from './select-multiple/select-multiple.schema';
import { selectForm } from './select/select.schema';
import { tableFormForm } from './table-form/table-form.schema';
import { tableForm } from './table/table.schema';
import { textForm } from './text/text.schema';
import { textareaForm } from './textarea/textarea.schema';

export const fieldsForm = recordSchema(
  {
    checkboxForm,
    checkboxgroupForm,
    dateForm,
    daterangeForm,
    numberForm,
    radioForm,
    selectForm,
    selectmultipleForm,
    tableForm,
    tableFormForm,
    textForm,
    textareaForm
  },
  { isForm: true }
);
