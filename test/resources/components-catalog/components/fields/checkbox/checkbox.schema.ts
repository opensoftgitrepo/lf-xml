import { booleanSchema, recordSchema } from '@lightweightform/storage';
import { fieldValidator } from '../../../schema.utils';

export const checkboxForm = recordSchema(
  {
    checkbox: booleanSchema({
      validator: fieldValidator
    }),
    checkboxComputed: booleanSchema({
      isNullable: true,
      computedValue: ctx => !ctx.get('checkbox')
    }),
    isValid: booleanSchema({ initialValue: true }),
    hasWarning: booleanSchema({ initialValue: false }),
    isReadOnly: booleanSchema({ initialValue: false })
  },
  { isForm: true }
);
