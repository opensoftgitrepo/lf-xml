import { booleanSchema, numberSchema, recordSchema, stringSchema } from '@lightweightform/storage';
import { fieldIsRequired, fieldValidator } from '../../../schema.utils';

export const radioForm = recordSchema(
  {
    radio: stringSchema({
      isNullable: true,
      isRequired: fieldIsRequired,
      validator: fieldValidator,
      allowedValues: ['A', 'B', 'C']
    }),
    radioComputed: stringSchema({
      isNullable: true,
      computedValue: ctx => (ctx.get('radio') !== null ? `${ctx.get('radio')}1` : (null as any))
    }),
    radioNumber: numberSchema({
      isNullable: true,
      allowedValues: [1, 2, 3]
    }),
    isValid: booleanSchema({ initialValue: true }),
    hasWarning: booleanSchema({ initialValue: false }),
    isReadOnly: booleanSchema({ initialValue: false }),
    isRequired: booleanSchema({ initialValue: true }),
    isInline: booleanSchema({ initialValue: false })
  },
  { isForm: true }
);
