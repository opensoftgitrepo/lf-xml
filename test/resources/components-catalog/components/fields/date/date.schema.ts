import { booleanSchema, dateSchema, recordSchema } from '@lightweightform/storage';
import { fieldIsRequired, fieldValidator } from '../../../schema.utils';

export const dateForm = recordSchema(
  {
    date: dateSchema({
      isNullable: true,
      validator: fieldValidator,
      isRequired: fieldIsRequired
    }),
    dateComputed: dateSchema({
      isNullable: true,
      computedValue: ctx => ctx.get('date')
    }),
    isValid: booleanSchema({ initialValue: true }),
    hasWarning: booleanSchema({ initialValue: false }),
    isReadOnly: booleanSchema({ initialValue: false }),
    isRequired: booleanSchema({ initialValue: false })
  },
  { isForm: true }
);
