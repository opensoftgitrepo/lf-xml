import { booleanSchema, recordSchema } from '@lightweightform/storage';
import { fieldIsRequired, fieldValidator } from '../../../schema.utils';
import { dateRangeSchema } from '../../../utils/date-range-schema';

export const daterangeForm = recordSchema(
  {
    daterange: dateRangeSchema({
      isRequired: fieldIsRequired,
      validator: fieldValidator,
      initialValue: [new Date('2019-01-01'), new Date('2019-02-02')]
    }),
    daterangeComputed: dateRangeSchema({
      computedValue: ctx => ctx.get('daterange')
    }),
    isValid: booleanSchema({ initialValue: true }),
    hasWarning: booleanSchema({ initialValue: false }),
    isReadOnly: booleanSchema({ initialValue: false }),
    isRequired: booleanSchema({ initialValue: false })
  },
  { isForm: true }
);
