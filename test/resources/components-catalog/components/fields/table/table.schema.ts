import {
  booleanSchema,
  listSchema,
  numberSchema,
  recordSchema,
  stringSchema
} from '@lightweightform/storage';
import { fieldValidator } from '../../../schema.utils';

export const tableForm = recordSchema(
  {
    table: listSchema(
      recordSchema({
        firstName: stringSchema({ minLength: 1 }),
        lastName: stringSchema(),
        age: numberSchema({
          isNullable: true,
          isInteger: true,
          min: 10,
          max: 120
        }),
        married: booleanSchema(),
        profession: stringSchema({
          isNullable: true,
          computedValue: ctx => {
            const age = ctx.get('age');
            return age === null ? (null as any) : age > 30 ? 'singer' : 'dancer';
          }
        })
      }),
      {
        validator: fieldValidator,
        initialValue: () => {
          const table: any = [];
          for (let i = 0; i < 10; ++i) {
            table[i] = {
              firstName: 'John',
              lastName: 'Smith',
              age: 42,
              married: false
            };
          }
          return table;
        }
      }
    ),
    averageAge: numberSchema({
      isNullable: true,
      computedValue: ctx =>
        ctx.get('table').reduce((avg, row) => avg + row.age, 0) / ctx.get('table').length
    }),
    numDancers: numberSchema({
      isNullable: true,
      computedValue: ctx =>
        (ctx.get('table').filter(row => row.profession === 'dancer') || []).length
    }),
    numSingers: numberSchema({
      isNullable: true,
      computedValue: ctx =>
        (ctx.get('table').filter(row => row.profession === 'singer') || []).length
    }),
    professionWithMorePeople: stringSchema({
      isNullable: true,
      computedValue: ctx =>
        ctx.get('numDancers') > ctx.get('numSingers')
          ? 'dancer'
          : ctx.get('numDancers') < ctx.get('numSingers')
            ? 'singer'
            : (null as any)
    }),
    isValid: booleanSchema({ initialValue: true }),
    hasWarning: booleanSchema({ initialValue: false }),
    isReadOnly: booleanSchema({ initialValue: false })
  },
  { isForm: true }
);
