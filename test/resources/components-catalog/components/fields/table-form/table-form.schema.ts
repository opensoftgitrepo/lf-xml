import { booleanSchema, listSchema, recordSchema, stringSchema } from '@lightweightform/storage';
import { fieldValidator } from '../../../schema.utils';

export const tableFormForm = recordSchema(
  {
    tableForm: listSchema(
      recordSchema({
        text1: stringSchema({ minLength: 1 }),
        text2: stringSchema(),
        text3: stringSchema()
      }),
      {
        validator: fieldValidator
      }
    ),
    isValid: booleanSchema({ initialValue: true }),
    hasWarning: booleanSchema({ initialValue: false }),
    isReadOnly: booleanSchema({ initialValue: false })
  },
  { isForm: true }
);
