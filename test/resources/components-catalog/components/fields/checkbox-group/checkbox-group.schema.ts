import {
  booleanSchema,
  listSchema,
  numberSchema,
  recordSchema,
  stringSchema
} from '@lightweightform/storage';
import { fieldValidator } from '../../../schema.utils';

export const checkboxgroupForm = recordSchema(
  {
    checkboxgroup: listSchema(stringSchema({ allowedValues: ['A', 'B', 'C'] }), {
      validator: fieldValidator
    }),
    checkboxgroupComputed: listSchema(stringSchema(), {
      computedValue: ctx => ctx.get('checkboxgroup'),
      isClientOnly: false
    }),
    checkboxgroupNumber: listSchema(numberSchema({ allowedValues: [1, 2, 3] })),
    isValid: booleanSchema({ initialValue: true }),
    hasWarning: booleanSchema({ initialValue: false }),
    isReadOnly: booleanSchema({ initialValue: false }),
    isInline: booleanSchema({ initialValue: false })
  },
  { isForm: true }
);
