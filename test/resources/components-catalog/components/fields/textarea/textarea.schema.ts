import { booleanSchema, recordSchema, stringSchema } from '@lightweightform/storage';
import { fieldValidator } from '../../../schema.utils';

export const textareaForm = recordSchema(
  {
    textarea: stringSchema({
      validator: fieldValidator,
      initialValue: 'ABC'
    }),
    textareaComputed: stringSchema({
      computedValue: ctx =>
        ctx
          .get('textarea')
          .split('')
          .reverse()
          .join('')
    }),
    isValid: booleanSchema({ initialValue: true }),
    hasWarning: booleanSchema({ initialValue: false }),
    isReadOnly: booleanSchema({ initialValue: false })
  },
  { isForm: true }
);
