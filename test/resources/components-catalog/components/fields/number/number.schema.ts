import { booleanSchema, numberSchema, recordSchema } from '@lightweightform/storage';
import { fieldIsRequired, fieldValidator } from '../../../schema.utils';

export const numberForm = recordSchema(
  {
    number: numberSchema({
      isNullable: true,
      validator: fieldValidator,
      isRequired: fieldIsRequired
    }),
    numberComputed: numberSchema({
      isNullable: true,
      computedValue: ctx => ctx.get('number') + 10
    }),
    scaleOption: numberSchema({
      isNullable: true,
      min: 0,
      initialValue: 2
    }),
    leadingZeroesOption: numberSchema({
      isNullable: true,
      min: 0,
      initialValue: 1
    }),
    isValid: booleanSchema({ initialValue: true }),
    hasWarning: booleanSchema({ initialValue: false }),
    isReadOnly: booleanSchema({ initialValue: false }),
    isRequired: booleanSchema({ initialValue: false })
  },
  { isForm: true }
);
