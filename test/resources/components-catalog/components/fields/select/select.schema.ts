import { booleanSchema, numberSchema, recordSchema, stringSchema } from '@lightweightform/storage';
import { fieldIsRequired, fieldValidator } from '../../../schema.utils';

export const selectForm = recordSchema(
  {
    select: stringSchema({
      isNullable: true,
      isRequired: fieldIsRequired,
      validator: fieldValidator,
      allowedValues: ['A', 'B', 'C']
    }),
    selectComputed: stringSchema({
      isNullable: true,
      computedValue: ctx => (ctx.get('select') !== null ? `${ctx.get('select')}1` : (null as any))
    }),
    selectNumber: numberSchema({
      isNullable: true,
      allowedValues: [1, 2, 3]
    }),
    isValid: booleanSchema({ initialValue: true }),
    hasWarning: booleanSchema({ initialValue: false }),
    isReadOnly: booleanSchema({ initialValue: false }),
    isRequired: booleanSchema({ initialValue: true }),
    isInline: booleanSchema({ initialValue: false })
  },
  { isForm: true }
);
