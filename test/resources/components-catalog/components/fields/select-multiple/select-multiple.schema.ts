import {
  booleanSchema,
  listSchema,
  numberSchema,
  recordSchema,
  stringSchema
} from '@lightweightform/storage';
import { fieldValidator } from '../../../schema.utils';

export const selectmultipleForm = recordSchema(
  {
    selectmultiple: listSchema(stringSchema({ allowedValues: ['A', 'B', 'C'] }), {
      validator: fieldValidator
    }),
    selectmultipleComputed: listSchema(stringSchema(), {
      computedValue: ctx => ctx.get('selectmultiple'),
      isClientOnly: false
    }),
    selectmultipleNumber: listSchema(numberSchema({ allowedValues: [1, 2, 3] })),
    isValid: booleanSchema({ initialValue: true }),
    hasWarning: booleanSchema({ initialValue: false }),
    isReadOnly: booleanSchema({ initialValue: false })
  },
  { isForm: true }
);
