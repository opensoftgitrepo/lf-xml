import { Storage, ValidationError } from '@lightweightform/storage';

export function fieldValidator(ctx: Storage) {
  const errors: ValidationError[] = [];
  if (!ctx.get('../isValid')) {
    errors.push({ code: 'CHECKED_IS_INVALID' });
  }
  if (ctx.get('../hasWarning')) {
    errors.push({ code: 'CHECKED_HAS_WARNING', isWarning: true });
  }
  return errors;
}

export function fieldIsRequired(ctx: Storage) {
  return ctx.get('isRequired');
}
