export const PLAYGROUND_HEADER = '<lf-playground>';

export const COMPUTED_HEADER = '<lf-computed>';
export const COMPONENT_OPTIONS = '<lf-options>';

export const I18N_STYLEGUIDE_DISCLAIMER_KEY = 'styleguideDisclaimerKey';

export const isValidField = {
  code: '2',
  label: 'Campo é válido?'
};
export const hasWarningField = {
  code: '3',
  label: 'Campo apresenta alerta?'
};
export const isReadOnlyField = {
  code: '4',
  label: 'Campo está readonly?'
};
export const isRequiredField = {
  code: '5',
  label: 'Campo é obrigatório?'
};

export const I18N_CHECKED_IS_INVALID = 'CHECKED_IS_INVALID';
