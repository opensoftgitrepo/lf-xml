export declare const PLAYGROUND_HEADER = '<lf-playground>';
export declare const COMPUTED_HEADER = '<lf-computed>';
export declare const COMPONENT_OPTIONS = '<lf-options>';
export declare const I18N_STYLEGUIDE_DISCLAIMER_KEY = 'styleguideDisclaimerKey';
export declare const isValidField: {
  code: string;
  label: string;
};
export declare const hasWarningField: {
  code: string;
  label: string;
};
export declare const isReadOnlyField: {
  code: string;
  label: string;
};
export declare const isRequiredField: {
  code: string;
  label: string;
};
export declare const I18N_CHECKED_IS_INVALID = 'CHECKED_IS_INVALID';
