import {
  DateSchema,
  dateSchema,
  DateSchemaOptions,
  isDateSchema,
  isTupleSchema,
  KnownKeys,
  ParentContextFunction,
  Schema,
  tupleSchema,
  TupleSchema
} from '@lightweightform/storage';

/**
 * Date-range schema.
 */
export interface DateRangeSchema extends TupleSchema<[Date, Date], [DateSchema, DateSchema]> {
  /**
   * Minimum date that the date-range represented by this schema is allowed to
   * have.
   */
  minDate?: Date | ParentContextFunction<Date>;
  /**
   * Maximum date that the date-range represented by this schema is allowed to
   * have.
   */
  maxDate?: Date | ParentContextFunction<Date>;
}

/**
 * Options used to create a new date-range schema.
 */
export type DateRangeSchemaOptions = Pick<
  DateRangeSchema,
  Exclude<KnownKeys<DateRangeSchema>, 'type' | 'elementsSchemas'>
> & { [extraProperties: string]: any };

/**
 * Creates a new date-range schema object.
 * @param options Options used to create the schema.
 * @returns Date-range schema.
 */
export function dateRangeSchema(options: DateRangeSchemaOptions = {}): DateRangeSchema {
  const startDateOptions: DateSchemaOptions = {};
  const endDateOptions: DateSchemaOptions = {};
  // When the date-range isn't computed, we apply `minDate` and `maxDate` to the
  // inner tuple elements for automatic validation. However, we need to fix the
  // storage path so that the user can write the `minDate` and `maxDate` as if
  // they were defined on the tuple itself
  if (options.computedValue === undefined) {
    const minDate =
      typeof options.minDate === 'function'
        ? ctx => ctx.schema().minDate(ctx.relativeStorage('..'))
        : options.minDate;
    const maxDate =
      typeof options.maxDate === 'function'
        ? ctx => ctx.schema().maxDate(ctx.relativeStorage('..'))
        : options.maxDate;
    startDateOptions.minDate = minDate;
    // Disallow an end date that comes before the start date
    endDateOptions.minDate = ctx => ctx.get('0');
    endDateOptions.maxDate = maxDate;
  }
  return tupleSchema([dateSchema(startDateOptions), dateSchema(endDateOptions)], {
    isNullable: true,
    ...options
  }) as DateRangeSchema;
}

/**
 * Returns whether a given schema is a date-range schema.
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is a date-range schema.
 */
export function isDateRangeSchema(schema: Schema): schema is DateRangeSchema {
  return (
    isTupleSchema(schema) &&
    schema.elementsSchemas.length === 2 &&
    isDateSchema(schema.elementsSchemas[0]) &&
    isDateSchema(schema.elementsSchemas[1])
  );
}
