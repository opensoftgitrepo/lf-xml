import { recordSchema } from '@lightweightform/storage';
import { fieldsForm } from './components/fields/fields.schema';
import { formListWithIdForm } from './components/form-list-with-id/form-list-with-id.schema';
import { formListForm } from './components/form-list/form-list.schema';
import { optionalWithIdForm } from './components/optional-with-id/optional-with-id.schema';
import { optionalForm } from './components/optional/optional.schema';

export const appSchema = recordSchema({
  fieldsForm,
  optionalForm,
  optionalWithIdForm,
  formListForm,
  formListWithIdForm
});
