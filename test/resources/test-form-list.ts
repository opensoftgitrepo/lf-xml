import { listSchema, recordSchema, stringSchema } from '@lightweightform/storage';

export default {
  schema: recordSchema(
    {
      formList: listSchema(
        recordSchema({
          text: stringSchema()
        }),
        { isFormList: true, maxSize: 3 }
      )
    },
    {
      initialValue: {
        formList: [{ text: 'bla' }, { text: 'foo' }]
      }
    }
  )
};
