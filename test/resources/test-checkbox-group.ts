import { listSchema, recordSchema, stringSchema } from '@lightweightform/storage';

export default {
  schema: recordSchema({
    record: recordSchema({
      a: listSchema(stringSchema({ allowedValues: ['1', '2', '3'] }), {
        initialValue: ['1', '2', '3']
      }),
      b: listSchema(stringSchema(), {
        computedValue: ctx => ctx.get('a'),
        isClientOnly: false
      })
    })
  })
};
