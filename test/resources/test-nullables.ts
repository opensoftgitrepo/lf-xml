import { listSchema, recordSchema, stringSchema } from '@lightweightform/storage';

export default {
  schema: recordSchema({
    record: recordSchema({
      a: stringSchema({ isNullable: true, allowedValues: ['A', 'B', 'C'], initialValue: null }),
      b: listSchema(stringSchema({ isNullable: true, allowedValues: ['A', 'B', 'C'] }))
    })
  })
};
