import { listSchema, recordSchema, stringSchema } from '@lightweightform/storage';

export default {
  schema: recordSchema({
    record: recordSchema({
      table: listSchema(
        recordSchema({
          name: stringSchema()
        }),
        { minSize: 2, maxSize: 5 }
      )
    })
  })
};
