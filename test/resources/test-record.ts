import { numberSchema, recordSchema } from '@lightweightform/storage';

export default {
  schema: recordSchema(
    {
      record: recordSchema({
        a: numberSchema()
      }),
      optionalRecord: recordSchema(
        {
          a: numberSchema()
        },
        { isNullable: true }
      )
    },
    {
      initialValue: {
        record: {
          a: 5
        }
      }
    }
  )
};
