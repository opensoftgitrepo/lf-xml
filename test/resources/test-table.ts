import {recordSchema, stringSchema, tableSchema} from '@lightweightform/storage';

export default {
  schema: recordSchema(
    {
      record: recordSchema({
        table: tableSchema(
          recordSchema({
            name: stringSchema()
          }),
          { minSize: 2, maxSize: 5,  initialValue: [{ name: 'bla' }, { name: 'foo' }, { name: 'john' }, { name: 'maria' }]}
        )
      })
    },
  )
};
