import LfXml from '../src/index';
import testRecord from './resources/test-record';
import testTable from './resources/test-table';
import testListTable from './resources/test-table-list';
import testFormLists from './resources/test-form-list';
import testCheckboxGroup from './resources/test-checkbox-group';
import testLeaf from './resources/test-leaf';
import testSimpleValue from './resources/test-simple-value';
import testAttributeMapping from './resources/test-attribute-mapping';
import testComputedValues from './resources/test-computed-values';
import testCustomParsersWriters from './resources/test-custom-writers-and-parsers';
import * as files from './resources/index';
import {getRowId, Storage} from '@lightweightform/storage';
import {LfFileStorage, LfI18n, LfStorage} from '@lightweightform/core';

function expectEqualXml(actual: string, expected: string) {
  expect(actual.replace(/\s/g, '')).toEqual(expected.replace(/\s/g, ''));
}

function initializeConfigutation(overrideConfig: any) {
  return new LfXml({
    ...CONFIGURATION,
    ...overrideConfig
  });
}

const CONFIGURATION = {
  rootName: 'LFXML',
  headerAttributes: {
    xmlns: 'xmlns-test',
    'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    version: '1',
    'xsi:schemaLocation': 'xsdlocation'
  },
  rowSuffix: '-linha',
  rowAttribute: 'linha',
  formListAttribute: 'id',
  formListSuffix: null,
  lfStorage: new Storage(testSimpleValue.schema)
};

const DEFAULT_I18N = { 'pt-PT': {} };

/**
 * Json2Xml test
 */
describe('Json2Xml test', () => {
  it('test if the inclusion of header attributes is working', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testRecord.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    let LFXML = initializeConfigutation({
      headerAttributes: undefined,
      lfStorage,
      lfI18n
    });
    expectEqualXml(
      LFXML.js2xmlString(lfStorage.getAsJSON()),
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML>
      <record>
      <a>5</a>
      </record>
      </LFXML>
      `
    );

    LFXML = initializeConfigutation({
      lfStorage,
      lfI18n
    });
    expectEqualXml(
      LFXML.js2xmlString(lfStorage.getAsJSON()),
      `
      <?xml version="1.0" encoding="UTF-8"?>
       <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
      <a>5</a>
      </record>
      </LFXML>
      `
    );
  });

  it('test if the parser of records is working', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testRecord.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    const LFXML = initializeConfigutation({
      lfStorage,
      lfI18n
    });
    const expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
      <a>5</a>
      </record>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
      <a>10</a>
      </record>
      <optionalRecord>
        <a>100</a>
      </optionalRecord>
      </LFXML>
      `;
    LFXML.xml2js(inputXML);
    expect(lfStorage.get('/record/a')).toEqual(10);
    expect(lfStorage.get('/optionalRecord/a')).toEqual(100);
  });

  it('test if the parser of records is working with other name', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testRecord.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    const LFXML = initializeConfigutation({
      lfStorage,
      lfI18n,
      mappings: {
        xmlName: { '/record': 'record_', '/record/a': 'b_' },
        jsName: { '/record_': 'record', '/record/b_': 'a' }
      }
    });
    const expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record_>
      <b_>5</b_>
      </record_>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record_>
      <b_>10</b_>
      </record_>
      <optionalRecord>
        <a>100</a>
      </optionalRecord>
      </LFXML>
      `;
    LFXML.xml2js(inputXML);
    expect(lfStorage.get('/record/a')).toEqual(10);
    expect(lfStorage.get('/optionalRecord/a')).toEqual(100);
  });

  it('test if the parser of tables with listSchema is working', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testListTable.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    const LFXML = initializeConfigutation({
      lfStorage,
      lfI18n
    });
    const expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
        <table>
          <table-linha linha="1">
            <name>bla</name>
          </table-linha>
          <table-linha linha="2">
            <name>foo</name>
          </table-linha>
          <table-linha linha="3">
            <name>john</name>
          </table-linha>
          <table-linha linha="4">
            <name>maria</name>
          </table-linha>
        </table>
      </record>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
        <table>
          <table-linha linha="1">
            <name>ABC</name>
          </table-linha>
          <table-linha linha="2">
            <name>DEF</name>
          </table-linha>
          <table-linha linha="3">
            <name>123</name>
          </table-linha>
          <table-linha linha="4">
            <name>456</name>
          </table-linha>
        </table>
      </record>
      </LFXML>
      `;
    LFXML.xml2js(inputXML);
    expect(lfStorage.get('record/table/0/name')).toEqual('ABC');
    expect(lfStorage.get('record/table/1/name')).toEqual('DEF');
    expect(lfStorage.get('record/table/2/name')).toEqual('123');
    expect(lfStorage.get('record/table/3/name')).toEqual('456');
  });

  it('test if the parser of tables with tableSchema is working', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testTable.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    const LFXML = initializeConfigutation({
      lfStorage,
      lfI18n
    });
    const expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
        <table>
          <table-linha linha="1">
            <name>bla</name>
          </table-linha>
          <table-linha linha="2">
            <name>foo</name>
          </table-linha>
          <table-linha linha="3">
            <name>john</name>
          </table-linha>
          <table-linha linha="4">
            <name>maria</name>
          </table-linha>
        </table>
      </record>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
        <table>
          <table-linha linha="1">
            <name>ABC</name>
          </table-linha>
          <table-linha linha="2">
            <name>DEF</name>
          </table-linha>
          <table-linha linha="3">
            <name>123</name>
          </table-linha>
          <table-linha linha="4">
            <name>456</name>
          </table-linha>
        </table>
      </record>
      </LFXML>
      `;
    LFXML.xml2js(inputXML);
    const table = lfStorage.get('record/table/');
    let rowId = getRowId(table[table.length - 1]) as number;
    expect(lfStorage.get(`record/table/${rowId - 3}/name`)).toEqual('ABC');
    expect(lfStorage.get(`record/table/${rowId - 2}/name`)).toEqual('DEF');
    expect(lfStorage.get(`record/table/${rowId - 1}/name`)).toEqual('123');
    expect(lfStorage.get(`record/table/${rowId}/name`)).toEqual('456');
  });

  it('test if the parser of tables is working with other name', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testTable.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    const LFXML = initializeConfigutation({
      lfStorage,
      lfI18n,
      mappings: {
        xmlName: { '/record/table': 'table_' },
        jsName: { '/record/table_': 'table' }
      }
    });
    const expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
        <table_>
          <table_-linha linha="1">
            <name>bla</name>
          </table_-linha>
          <table_-linha linha="2">
            <name>foo</name>
          </table_-linha>
          <table_-linha linha="3">
            <name>john</name>
          </table_-linha>
          <table_-linha linha="4">
            <name>maria</name>
          </table_-linha>
        </table_>
      </record>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
        <table_>
          <table_-linha linha="1">
            <name>ABC</name>
          </table_-linha>
          <table_-linha linha="2">
            <name>DEF</name>
          </table_-linha>
          <table_-linha linha="3">
            <name>123</name>
          </table_-linha>
          <table_-linha linha="4">
            <name>456</name>
          </table_-linha>
        </table_>
      </record>
      </LFXML>
      `;
    LFXML.xml2js(inputXML);
    const table = lfStorage.get('record/table/');
    let rowId = getRowId(table[table.length - 1]) as number;
    expect(lfStorage.get(`record/table/${rowId - 3}/name`)).toEqual('ABC');
    expect(lfStorage.get(`record/table/${rowId - 2}/name`)).toEqual('DEF');
    expect(lfStorage.get(`record/table/${rowId - 1}/name`)).toEqual('123');
    expect(lfStorage.get(`record/table/${rowId}/name`)).toEqual('456');
  });

  it('test if the parser of form lists is working', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testFormLists.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    let LFXML = initializeConfigutation({
      lfStorage,
      lfI18n
    });
    let expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <formList id="1">
        <text>bla</text>
      </formList>
      <formList id="2">
        <text>foo</text>
      </formList>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <formList id="1">
        <text>ABC</text>
      </formList>
      <formList id="2">
        <text>DEF</text>
      </formList>
      </LFXML>
      `;
    LFXML.xml2js(inputXML);
    expect(lfStorage.get('formList/0/text')).toEqual('ABC');
    expect(lfStorage.get('formList/1/text')).toEqual('DEF');

    lfStorage.reset('/');
    LFXML = initializeConfigutation({
      lfStorage,
      formListAttribute: null
    });
    expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <formList>
        <text>bla</text>
      </formList>
      <formList>
        <text>foo</text>
      </formList>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);

    LFXML = initializeConfigutation({
      lfStorage,
      formListSuffix: '-entry',
      formListAttribute: null
    });
    expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <formList-entry>
        <text>bla</text>
      </formList-entry>
      <formList-entry>
        <text>foo</text>
      </formList-entry>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
  });

  it('test if the parser of form lists is working with other name', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testFormLists.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    let LFXML = initializeConfigutation({
      lfStorage,
      lfI18n,
      mappings: {
        xmlName: { '/formList': 'formList_' },
        jsName: { '/formList_': 'formList' }
      }
    });
    let expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <formList_ id="1">
        <text>bla</text>
      </formList_>
      <formList_ id="2">
        <text>foo</text>
      </formList_>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <formList_ id="1">
        <text>ABC</text>
      </formList_>
      <formList_ id="2">
        <text>DEF</text>
      </formList_>
      </LFXML>
      `;
    LFXML.xml2js(inputXML);
    expect(lfStorage.get('formList/0/text')).toEqual('ABC');
    expect(lfStorage.get('formList/1/text')).toEqual('DEF');

    lfStorage.reset('/');
    LFXML = initializeConfigutation({
      lfStorage,
      formListAttribute: null,
      lfI18n,
      mappings: {
        xmlName: { '/formList': 'formList_' },
        jsName: { '/formList_': 'formList' }
      }
    });
    expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <formList_>
        <text>bla</text>
      </formList_>
      <formList_>
        <text>foo</text>
      </formList_>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);

    LFXML = initializeConfigutation({
      lfStorage,
      formListSuffix: '-entry',
      formListAttribute: null,
      lfI18n,
      mappings: {
        xmlName: { '/formList': 'formList_' },
        jsName: { '/formList_': 'formList' }
      }
    });
    expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <formList_-entry>
        <text>bla</text>
      </formList_-entry>
      <formList_-entry>
        <text>foo</text>
      </formList_-entry>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
  });

  it('test if the parser of leafs is working', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testLeaf.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    const LFXML = initializeConfigutation({
      lfStorage,
      lfI18n
    });
    const expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
      <a>5</a>
      <b>bla</b>
      <c>true</c>
      <d></d>
      <e></e>
      </record>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
      <a>10</a>
      <b>abc</b>
      <c>false</c>
      <d></d>
      <e></e>
      </record>
      </LFXML>
      `;
    LFXML.xml2js(inputXML);
    expect(lfStorage.get('record/a')).toEqual(10);
    expect(lfStorage.get('record/b')).toEqual('abc');
    expect(lfStorage.get('record/c')).toEqual(false);
    expect(lfStorage.get('record/d')).toEqual(null);
    expect(lfStorage.get('record/e')).toEqual(null);
  });

  it('test if the parser of checkbox-groups is working', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testCheckboxGroup.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    const LFXML = initializeConfigutation({
      lfStorage,
      lfI18n
    });
    const expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
      <a>
        <a-linha linha="1">1</a-linha>
        <a-linha linha="2">2</a-linha>
        <a-linha linha="3">3</a-linha>
      </a>
      <b>
        <b-linha linha="1">1</b-linha>
        <b-linha linha="2">2</b-linha>
        <b-linha linha="3">3</b-linha>
      </b>
      </record>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
      <a>
        <a-linha linha="1">1</a-linha>
        <a-linha linha="2">3</a-linha>
      </a>
      <b>
        <b-linha linha="1">1</b-linha>
        <b-linha linha="2">3</b-linha>
      </b>
      </record>
      </LFXML>
      `;
    LFXML.xml2js(inputXML);
    expect(lfStorage.get('record/a')).toEqual(['1', '3']);
    expect(lfStorage.get('record/b')).toEqual(['1', '3']);
  });

  it('test if the parser of checkbox-groups is working with other name', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testCheckboxGroup.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    const LFXML = initializeConfigutation({
      lfStorage,
      lfI18n,
      mappings: {
        xmlName: { '/record/a': 'a_' },
        jsName: { '/record/a_': 'a' }
      }
    });
    const expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
      <a_>
        <a_-linha linha="1">1</a_-linha>
        <a_-linha linha="2">2</a_-linha>
        <a_-linha linha="3">3</a_-linha>
      </a_>
      <b>
        <b-linha linha="1">1</b-linha>
        <b-linha linha="2">2</b-linha>
        <b-linha linha="3">3</b-linha>
      </b>
      </record>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
      <a_>
        <a_-linha linha="1">1</a_-linha>
        <a_-linha linha="2">3</a_-linha>
      </a_>
      <b>
        <b-linha linha="1">1</b-linha>
        <b-linha linha="2">3</b-linha>
      </b>
      </record>
      </LFXML>
      `;
    LFXML.xml2js(inputXML);
    expect(lfStorage.get('record/a')).toEqual(['1', '3']);
    expect(lfStorage.get('record/b')).toEqual(['1', '3']);
  });

  it('test if js parser is working (complete lf-app)', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testSimpleValue.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, testSimpleValue.lfI18n, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    const LFXML = initializeConfigutation({
      lfStorage,
      lfI18n
    });
    const expectedXml = `<?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
        <fields>
          <checkbox>
            <checkbox>true</checkbox>
          </checkbox>
          <table>
            <table>
              <table-linha linha="1">
                <firstName>John</firstName>
                <lastName>Smith</lastName>
                <age></age>
                <married>true</married>
                <profession>singer</profession>
              </table-linha>
              <table-linha linha="2">
                <firstName>Marie</firstName>
                <lastName>Currie</lastName>
                <age>50</age>
                <married>false</married>
                <profession>dancer</profession>
              </table-linha>
            </table>
          </table>
        </fields>
        <formList id="ABC">
          <text>ABC</text>
        </formList>
        <formList id="Foo">
          <text>Foo</text>
        </formList>
        <formList id="bar">
          <text>bar</text>
        </formList>
      </LFXML>`;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `<?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
        <fields>
          <checkbox>
            <checkbox>true</checkbox>
          </checkbox>
          <table>
            <table>
              <table-linha linha="1">
                <firstName>Marie</firstName>
                <lastName>Curie</lastName>
                <age>25</age>
                <married>false</married>
                <profession>dancer</profession>
              </table-linha>
            </table>
          </table>
        </fields>
        <formList id="Foo">
          <text>Foo</text>
        </formList>
        <formList id="Bar">
          <text>Bar</text>
        </formList>
      </LFXML>`;
    LFXML.xml2js(inputXML);
    expect(lfStorage.size('fields/table/table')).toEqual(1);
    expect(lfStorage.get('fields/table/table/0/firstName')).toEqual('Marie');
    expect(lfStorage.get('fields/table/table/0/lastName')).toEqual('Curie');
    expect(lfStorage.get('fields/table/table/0/age')).toEqual(25);
    expect(lfStorage.get('fields/table/table/0/married')).toEqual(false);
    expect(lfStorage.get('fields/table/table/0/profession')).toEqual('dancer');
    expect(lfStorage.size('formList')).toEqual(2);
    expect(lfStorage.get('formList/0/text')).toEqual('Foo');
    expect(lfStorage.get('formList/1/text')).toEqual('Bar');
  });

  it('test if js parser is working (complete lf-app) with other name', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testSimpleValue.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, testSimpleValue.lfI18n, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    const LFXML = initializeConfigutation({
      lfStorage,
      lfI18n,
      mappings: {
        xmlName: {
          '.*/firstName': 'firstName_',
          '/formList/\\d/text': 'text_',
        },
        jsName: {
          '/fields/table/table/\\d/firstName_': 'firstName',
          '/formList/\\d/text_': 'text'
        }
      }
    });
    const expectedXml = `<?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
        <fields>
          <checkbox>
            <checkbox>true</checkbox>
          </checkbox>
          <table>
            <table>
              <table-linha linha="1">
                <firstName_>John</firstName_>
                <lastName>Smith</lastName>
                <age></age>
                <married>true</married>
                <profession>singer</profession>
              </table-linha>
              <table-linha linha="2">
                <firstName_>Marie</firstName_>
                <lastName>Currie</lastName>
                <age>50</age>
                <married>false</married>
                <profession>dancer</profession>
              </table-linha>
            </table>
          </table>
        </fields>
        <formList id="ABC">
          <text_>ABC</text_>
        </formList>
        <formList id="Foo">
          <text_>Foo</text_>
        </formList>
        <formList id="bar">
          <text_>bar</text_>
        </formList>
      </LFXML>`;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `<?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
        <fields>
          <checkbox>
            <checkbox>true</checkbox>
          </checkbox>
          <table>
            <table>
              <table-linha linha="1">
                <firstName_>Marie</firstName_>
                <lastName>Curie</lastName>
                <age>25</age>
                <married>false</married>
                <profession>dancer</profession>
              </table-linha>
            </table>
          </table>
        </fields>
        <formList id="Foo">
          <text_>Foo</text_>
        </formList>
        <formList id="Bar">
          <text_>Bar</text_>
        </formList>
      </LFXML>`;
    LFXML.xml2js(inputXML);
    expect(lfStorage.size('fields/table/table')).toEqual(1);
    expect(lfStorage.get('fields/table/table/0/firstName')).toEqual('Marie');
    expect(lfStorage.get('fields/table/table/0/lastName')).toEqual('Curie');
    expect(lfStorage.get('fields/table/table/0/age')).toEqual(25);
    expect(lfStorage.get('fields/table/table/0/married')).toEqual(false);
    expect(lfStorage.get('fields/table/table/0/profession')).toEqual('dancer');
    expect(lfStorage.size('formList')).toEqual(2);
    expect(lfStorage.get('formList/0/text')).toEqual('Foo');
    expect(lfStorage.get('formList/1/text')).toEqual('Bar');
  });

  it("test attribute mapping for forms with attribute's id", () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testAttributeMapping.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, testAttributeMapping.lfI18n, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    const LFXML = initializeConfigutation({
      lfStorage,
      lfI18n
    });
    const expectedXml = `<?xml version="1.0" encoding="UTF-8"?>
    <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
     <record>
      <a>5</a>
     </record>
     <formList id="A1"/>
     <formList id="B1"/>
    </LFXML>`;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    expect(lfStorage.get('record/a')).toEqual(5);
    expect(lfStorage.get('formList/0/formInitializer/formId')).toEqual('A');
    expect(lfStorage.get('formList/1/formInitializer/formId')).toEqual('B');

    const xml = `<?xml version="1.0" encoding="UTF-8"?>
    <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
     <record>
      <a>5</a>
     </record>
     <formList id="C"/>
     <formList id="D"/>
    </LFXML>`;
    LFXML.xml2js(xml);
    expect(lfStorage.get('formList/0/formInitializer/formId')).toEqual('C');
    expect(lfStorage.get('formList/1/formInitializer/formId')).toEqual('D');
  });

  it('test if custom writers and parsers are working', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testCustomParsersWriters.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    const LFXML = initializeConfigutation({
      lfStorage,
      lfI18n
    });
    const expectedXml = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
      <value>123.45</value>
      <record>bla</record>
      <list><text row="1">foo</text><text row="2">bar</text></list>
      <checkboxGroup>a,b,c</checkboxGroup>
      <dateRange>2019-02-02 - 2019-01-01</dateRange>
      </record>
      </LFXML>
      `;
    expectEqualXml(LFXML.js2xmlString(lfStorage.getAsJSON()), expectedXml);
    const inputXML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
      <value>123.45</value>
      <record>foo</record>
      <list><text row="1">John</text><text row="2">Smith</text></list>
      <checkboxGroup>a,b,c</checkboxGroup>
      <dateRange>2019-02-02 - 2019-01-01</dateRange>
      </record>
      </LFXML>
      `;
    LFXML.xml2js(inputXML);
    expect(lfStorage.get('record/value')).toEqual(12345);
    expect(lfStorage.get('record/record/subRecord')).toEqual('foo');
    expect(lfStorage.get('record/list/0/text')).toEqual('John');
    expect(lfStorage.get('record/list/1/text')).toEqual('Smith');
    expect(lfStorage.get('record/checkboxGroup')).toEqual(['a','b','c']);
    expect(lfStorage.get('record/dateRange')).toEqual([new Date(2019,0,1), new Date(2019,1,2)]);
  });

  it('test if the computed values are ignored', () => {
    const lfFileStorage = new LfFileStorage();
    const lfStorage = new LfStorage(testComputedValues.schema, lfFileStorage);
    const lfI18n = new LfI18n(lfStorage, DEFAULT_I18N, null, null);
    lfI18n.currentLanguage = 'pt-PT';
    let LFXML = initializeConfigutation({
      lfStorage
    });
    const inputXML = `
      <?xml version="1.0" encoding="UTF-8"?>
      <LFXML xmlns="xmlns-test" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="xsdlocation">
      <record>
        <text>A</text>
      </record>
      <record2>
        <number>5</number>
      </record2>
      <formList id="1">
        <text>bla</text>
      </formList>
      <formList id="2">
        <text>foo</text>
      </formList>
      </LFXML>
      `;
    LFXML.xml2js(inputXML);
    expect(lfStorage.get('record/text')).toEqual('original');
    expect(lfStorage.get('record2/number')).toEqual(10);
    expect(lfStorage.get('formList/0/text')).toEqual('A');
    expect(lfStorage.get('formList/1/text')).toEqual('B');
  });

  it('test both transformations over multiple files', () => {
    Object.keys(files).forEach((key: string) => {
      const file: any = (files as any)[key];
      const lfFileStorage = new LfFileStorage();
      const lfStorage = new LfStorage(file.schema, lfFileStorage);
      const lfI18n = new LfI18n(lfStorage, file.lfI18n || DEFAULT_I18N, null, null);
      lfI18n.currentLanguage = 'pt-PT';
      const LFXML = initializeConfigutation({
        lfStorage,
        lfI18n,
        rootName: file.rootName || CONFIGURATION.rootName,
        headerAttributes: file.headerAttributes || CONFIGURATION.headerAttributes,
        rowSuffix: file.rowSuffix && file.rowSuffix !== null ? CONFIGURATION.rowSuffix : null,
        rowAttribute:
          file.rowAttribute && file.rowAttribute !== null ? CONFIGURATION.rowAttribute : null,
        formListAttribute:
          file.formListAttribute && file.formListAttribute !== null
            ? CONFIGURATION.formListAttribute
            : null,
        formListSuffix:
          file.formListSuffix && file.formListSuffix !== null ? CONFIGURATION.formListSuffix : null
      });
      const original = lfStorage.getAsJSON();
      const xml = LFXML.js2xmlString(lfStorage.getAsJSON());
      LFXML.xml2js(xml);
      expect(original).toEqual(lfStorage.getAsJSON());
    });
  });
});
